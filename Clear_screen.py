from IPython.display import clear_output

def clear():
    clear_output(wait=True)
